### Steps to Deploy
- move contents of frontend/ to /srv/www-csc-links/
- create a `.env` file in backend/ with following contents:
```
PASSWORD=...
PORT=...
```
- run the following in backend/:
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
- run python app in backend/ (with the virtual env activated)
- edit the `.htaccess` file in /srv/www-csc-links/ to point to the running python application
