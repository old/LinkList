import type { AppProps } from "next/app";
import React from "react";
import "styles/globals.css";
import Head from "next/head";

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => (
  <>
    <Head>
      <title>@uwcsclub | LinkList</title>
    </Head>
    <Component {...pageProps} />
  </>
);

export default MyApp;
