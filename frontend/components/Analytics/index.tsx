import React from "react";

interface AnalyticsProps {
  clicks: number;
}

const Analytics: React.FC<AnalyticsProps> = ({ clicks }) => {
  return (
    <div className="w-full h-12 lt-lg:h-16 mr-0 px-4 lt-lg:px-6 flex border-b border-gray-300 text-sm font-karla">
      <div className="w-full h-full flex-analytics flex items-center">
        <span className="mr-4 font-bold">Lifetime Analytics:</span>
        <div className="mr-8 flex justify-center items-center">
          <div className="h-2 w-2 mr-2 rounded bg-analytics-click-icon"></div>
          <div className="flex text-xs lt-lg:text-base font-normal">
            <p className="whitespace-pre">Clicks: </p>
            <p className="font-bold lt-lg:font-normal">{clicks || "-"}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Analytics;
