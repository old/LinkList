import React from "react";

export interface Link {
  name: string;
  url: string;
}
interface Props {
  links: Link[];
}

export const Links: React.FC<Props> = ({ links }) => {
  return (
    <div className="text-s flex flex-col items-center w-full top-6 font-karla">
      <img className="mb-3" src="images/csc-logo.png" alt="CSC Logo" width="100px" />
      <h1 className="font-bold">@uwcsclub</h1>
      <ul className="flex flex-col my-6 w-full">
        {links.map(({ name, url }, i) => (
          <li key={i} className="w-full flex justify-center">
            <a
              className="btn bg-gray-450 p-3 text-white font-karla font-bold text-center self-center my-1.5
        hover:bg-white hover:text-black border-2 border-gray-800 transition duration-300 ease-in-out
        w-11/12 sm:w-4/12 min-h-link"
              href={url}
              target="_blank"
              rel="noreferrer"
            >
              {name}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};
