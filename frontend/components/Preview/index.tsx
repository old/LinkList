import { Link, Links } from "components/Links";
import React from "react";
import styles from "./styles.module.css";

interface PreviewProps {
  links: Link[];
}

const Preview: React.FC<PreviewProps> = ({ links }) => {
  return (
    <div className={styles.container}>
      <div
        className={`box-border bg-white rounded-preview border-preview border-black ${styles.parent}`}
      >
        <div className={styles.child}>
          <Links links={links} />
        </div>
      </div>
    </div>
  );
};

export default Preview;
