// @ts-check

/* eslint-disable @typescript-eslint/no-var-requires */

// eslint-disable-next-line no-undef
const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");

const devConfig = {
  async rewrites() {
    return [
      {
        source: "/api",
        destination: "http://localhost:5000",
      },
      {
        source: "/api/:path*",
        destination: "http://localhost:5000/:path*",
      },
    ];
  },
};

const prodConfig = {
  basePath: "/links",
};

// eslint-disable-next-line no-undef
module.exports = (phase) =>
  phase === PHASE_DEVELOPMENT_SERVER ? devConfig : prodConfig;
