## Architecture

![client-server interaction graph](./assets/client-server-interaction.svg)

## Dependencies

1. Node.js
1. npm
1. Python 3.6+

## Setup

For setting up the frontend, `setup.sh` will run `npm install` in the `frontend` folder.

For setting up the backend, `setup.sh` will initialize a new virtual environment and setup a dummy sqlite file for testing purposes.

## Dev

Run `setup.sh` and then run `dev.sh`

## Production

TODO